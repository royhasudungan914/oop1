<?php

require_once 'Animal.php';
require_once 'Ape.php';
require_once 'Frog.php';

$sheep = new Animal("shaun");

echo "<h2>Release 0</h2>";
echo "Name :" . $sheep->get_name() . "<br>"; // "shaun"
echo "Legs:" . $sheep->legs . "<br>"; // 4
echo "cold blooded: no"; 


$sungokong = new Ape("kera sakti");

echo "<h2>Release 1</h2>";
echo "Name : " . $sungokong->get_name() . "<br>";
echo "legs : 2 ";
echo "<br>";
echo "cold blooded : no"; 
echo "<br>";
echo "Yell : " . $sungokong->yell(); // "Auooo"
echo "<br> <br>";

$kodok = new Frog("buduk");
echo "Name : " . $kodok->get_name() . "<br>";
echo "legh : 4";
echo "<br>";
echo "cold blooded :no";
echo "<br>";
echo "jump : hop hop";

?>